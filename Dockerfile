FROM php:7.4-fpm-buster

# install the PHP extensions we need
RUN set -ex; \
    \
    apt-get update; \
    apt-get install -y \
        git \
        python \
        python-pip \
        python-jinja2 \
        openssh-client \
        libjpeg-dev \
        libpng-dev \
        libfreetype6-dev \
        libjpeg62-turbo-dev \
        libmcrypt-dev \
        msmtp \
        git \
        libzip-dev \
        libzip4 \
        zip \
        libxml2-dev \
        zlib1g-dev \
        zip \
        unzip \
        libpng-dev \
        libmemcached-dev \
        default-libmysqlclient-dev \
        libicu-dev \
        libpq-dev \
        curl \
        libmagickwand-dev \
        telnet \
    ; \
    rm -rf /var/lib/apt/lists/*; \
    \
    docker-php-ext-install gd soap bcmath mysqli opcache pdo_mysql pdo_pgsql json zip;


#Redis
RUN { \
    mkdir -p /usr/src/php/ext/redis; \
    curl -L https://github.com/phpredis/phpredis/archive/3.0.0.tar.gz | tar xvz -C /usr/src/php/ext/redis --strip 1; \
    echo 'redis' >> /usr/src/php-available-exts; \
    docker-php-ext-install redis; \
}

#Imagick
RUN { \
    pecl install imagick; \
    docker-php-ext-enable imagick; \
}

#Install Composer
RUN { \
    php -r "copy('https://getcomposer.org/installer', '/tmp/composer-setup.php');"; \
    php /tmp/composer-setup.php --install-dir=/usr/local/bin --filename=composer; \
    mkdir -p /cache/composer; \
    }

COPY ./www.conf /usr/local/etc/php-fpm.d/
